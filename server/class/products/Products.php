<?php

include($_SERVER['DOCUMENT_ROOT'] . '/server/lib/database.php');
include($_SERVER['DOCUMENT_ROOT']  . '/server/config/config.php');

class Products {

	public $db;

	function __construct() {
		$this->db = new Database(DB_NAME, DB_HOSTNAME, DB_USER, DB_PASSWORD);
	}

	public function getProducts($limit) {
		$query = '
			SELECT
				p.id,
				p.name,
				p.description,
				p.images,
				p.creation_date,
				t.type_name,
				t.id as type_id
			FROM products p
			INNER JOIN product_type t
			ON p.type = t.id
			LIMIT ?';
		$result = $this->db->fetch_rows($query, true, array($limit));

		return $result;
	}
	
	public function getSingleProduct($id) {
		$query = '
			SELECT
				p.id,
				p.name,
				p.description,
				p.images,
				p.creation_date,
				t.type_name,
				t.id as type_id
			FROM products p
			INNER JOIN product_type t
			ON p.type = t.id
			WHERE p.id = ?';
		$result = $this->db->fetch_rows($query, true, array($id));
		$result[0]->description = htmlspecialchars_decode($result[0]->description);
		$result[0]->images = $this->getProductImages($id);

		return $result;
	}
	
	private function getProductImages($id) {
		$folder = $_SERVER['DOCUMENT_ROOT'] . '/build/images/products/' . $id;
		$images = array_diff(scandir($folder), array('.', '..'));
		$result = [];
		foreach($images as $file) {
			$object = new stdClass();
			$object->isMain = false;
			if (strpos($file, 'main') === 0) {
				$object->isMain = true;
			}
			$path = $folder . '/' . $file;
			$type = pathinfo($path, PATHINFO_EXTENSION);
			$data = file_get_contents($path);
			$object->file = 'data:image/' . $type . ';base64,' . base64_encode($data);
			$result[] = $object;
		}
		
		return $result;
	}

	public function createNew($params) {
		$cleanDescription = htmlspecialchars($params->description);
		$query = '
			INSERT INTO products
			(type, name, description, creation_date)
			VALUES
			(?, ?, ?, NOW())';
		$sql_result = $this->db->query($query, array(
			$params->type_id,
			$params->name,
			$cleanDescription
		));
		$sql_result = true;
		if ($sql_result) {
			$img_result = $this->manageImages($this->db->insert_id, $params->productsImages);
		}
		return $img_result;
		if ($img_result) {
			return true;
		}
	}
	
	
 
	public function update($params) {
		$query = '
			UPDATE products SET
			type = ?,
			name = ?,
			description = ?
			WHERE id = ?';
		$sql_result = $this->db->query($query, array(
			$params->type_id,
			$params->name,
			$params->description,
			$params->id
		));
		$this->deleteImagesFiles($params->id);
		if ($sql_result) {
			$img_result = $this->manageImages($params->id, $params->productsImages);
			if ($img_result) {
				$saved = true;
			}
		} else if($sql_result) {
			$saved = true;
		} else {
			$saved = false;
		}

		return $saved;
	}
	
	private function deleteImagesFiles($id) {
		$files = glob($_SERVER['DOCUMENT_ROOT'] . '/build/images/products/' . $id . '/*');
		foreach($files as $file){
			if(is_file($file)) {
				$result = unlink($file);
				if (!$result) {
					break;
				}
			}
		}
		
		return $result;
	}

	public function delete($params) {
		$query = 'DELETE FROM products WHERE id = ?';
		$result = $this->db->query($query, array($params->id));
		if ($result) { $this->deleteImagesFiles($params->id); };
		return $result;
	}
	
	private function manageImages($productId, $images) {
		foreach ($images as $img) {
			$img->isMain === true ? $imgName = 'main' : $imgName = time() . '-' . rand(10,100);
			$saveResult = $this->saveImg($productId, $img->file, $imgName);
			if (!$saveResult) {$folder = $_SERVER['DOCUMENT_ROOT'] . '/build/images/products/' . $id;
				return false;
			}
		}
		
		return true;
	}
	
	private function saveImg($id, $img, $file_name) {
		$data = $img;

		list($type, $data) = explode(';', $data);
		list(, $data)      = explode(',', $data);
		$data = base64_decode($data);
		$folder = $_SERVER['DOCUMENT_ROOT'] . '/build/images/products/' . $id;
		if (!is_dir($folder)) {
			if (!mkdir($folder, 0775, true)) {
				$error = error_get_last();
    			return $error['message'];
			}
		}
		$file = $folder . '/' . $file_name . '.png';
		if (file_exists($file)) {
			unlink($file);
		}
		$result = file_put_contents($file, $data);
		chmod($folder . '/' . $file_name . '.png', 775);

		return $result;
	}	
}
?>