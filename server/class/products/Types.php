<?php 

include($_SERVER['DOCUMENT_ROOT'] . '/server/lib/database.php');
include($_SERVER['DOCUMENT_ROOT']  . '/server/config/config.php');

class Types {
    
    public $db;

	function __construct() {
		$this->db = new Database(DB_NAME, DB_HOSTNAME, DB_USER, DB_PASSWORD);
	}
    
    public function getTypes() {
		return $this->db->fetch_rows('SELECT * FROM product_type', true, array());
	}
    
    public function createTypes($name) {
        $query = 'INSERT INTO product_type (type_name) VALUES (?)';
        $result = $this->db->query($query, array($name));
        
        return $result;
    }
    
    public function deleteTypes($id) {
        $query = 'DELETE FROM product_type WHERE id = ?';
        $result = $this->db->query($query, array($id));
        
        return $result;
    }

    public function editType($params) {
        $query = 'UPDATE product_type SET type_name = ? WHERE id = ?';
        $result = $this->db->query($query, array($params->name, $params->id));
        
        return $result;
    }
}

?>