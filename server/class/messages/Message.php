<?php

include($_SERVER['DOCUMENT_ROOT'] . '/server/lib/database.php');
include($_SERVER['DOCUMENT_ROOT'] . '/server/config/config.php');

class Message {
    
    // public $db;

	function __construct() {
		$this->db = new Database(DB_NAME, DB_HOSTNAME, DB_USER, DB_PASSWORD);
	}

    public function save_to_db($msg_data) {
		$msg_params = $this->encode_msg_params($msg_data);
		
        $query = '
            INSERT INTO messages (
                email,
                name,
                message,
				messageParams,
                send_time
            ) VALUES (?, ?, ?, ?, now())
        ';

        $result =  $this->db->query($query, array(
            $msg_data->email,
            $msg_data->name,
            $msg_data->message,
			$msg_params
        ));

        return $result;
    }
	
	public function get_all_messages() {
		
		$query = 'SELECT * FROM messages ORDER BY `send_time`';
		$result = $this->db->fetch_rows($query, true, array());
		
		return $result;
	}
	
	public function get_single_message($msgId) {
		$query = 'SELECT * FROM messages WHERE id = ?';
		$result = $this->db->fetch_row($query, true, array($msgId));
		
		return $result;
	}
	
	private function encode_msg_params($data) {
		
		$msg_params = new stdClass();
		$msg_params->alergies = $data->alergies;
		$msg_params->flavors = $data->flavors;
		$msg_params->layers = $data->layers;
		$msg_params->numb_guests = $data->numb_guests;
		$msg_params->requiredDate = $data->date;
		
		$msg_params = json_encode($msg_params);
		
		return $msg_params;
	}

}

?>