<?php

include($_SERVER['DOCUMENT_ROOT'] . '/server/config/config.php');
include($_SERVER['DOCUMENT_ROOT'] . '/server/lib/database.php');
include($_SERVER['DOCUMENT_ROOT'] . '/server/lib/jwt.php');

class UserAuth {

    public $email;
    public $password;
    public $db;

    function __construct($req_email, $req_password) {
        $this->email = $req_email;
        $this->password = $req_password;
        $this->db = new Database(DB_NAME, DB_HOSTNAME, DB_USER, DB_PASSWORD);
    }

    public function login(){

        $query = "SELECT id, email, firstname, lastname, type, password FROM users WHERE email = ?";
        $user = $this->db->fetch_row($query, true, array($this->email));
        if(!$user){ return false; }

        $check_password = $this->validate_password($this->password, $user->password);
        if(!$check_password){ return false; }
        unset($user->password);

        $user->AuthJwt = $this->createJwt($user);

        return $user;
    }

    private function validate_password($req_password, $user_password) {
        $password = hash('SHA256', $req_password);
        if ($password !== $user_password) {
            return false;
        } else {
            return true;
        }
    }

    private function createJwt($user) {
        $header = new StdClass;
        $header->typ = 'JWT';
        $header->alg = JWT_ALG;

        $payload = json_encode($user);

        $key = JWT_KEY;

        $c_jwt = new Jwt;

        $token = $c_jwt->encode(json_encode($header), $payload, $key);

        return $token;
    }
}
?>