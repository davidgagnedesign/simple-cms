<?php
    error_reporting(E_ALL & ~E_NOTICE & ~E_WARNING);
    // error_reporting(E_ALL);
    
    // Includes required modules and lib
    require('../config.php');
    require('../../lib/database.php');

    /***************************************************
    LOGIC
    ***************************************************/
     // Ask for version
    echo "What version of the site would you like to deploy ?";
    $version = trim(fgets(STDIN));
    
    if($version === 'init') { 
        echo "Installing version \"inti\" will reset the database. Are you sure ? (yes/no)\n";
        $confirmation = trim(fgets(STDIN));
        if($confirmation === 'no') { exit("Migration script exited!\n"); }
    }

    echo "Version to deploy :" . $version . "\n";

    // Get version config object 
    $config = json_decode(file_get_contents('config/' . $version . '.json'));

    if(!$config){exit("No config file \"". $version ."\" were found\n");}
    echo "You are about to deploy database version " . $version . " Continue ? (yes/no)\n";
    $confirmation = trim(fgets(STDIN));
    if($confirmation === 'no'){ exit("Migration script exited!\n"); }

    // Connect to data base
    $c_db = new Database(DB_NAME, DB_HOSTNAME, DB_USER, DB_PASSWORD);
    
    // Get drop table config and execute
    echo "\n\n\n\nDrop task started\n";
    $drop_result = dropTables($config->drop);
    echo $drop_result;
    echo "Drop task finished\n\n\n\n";

    // Alter table
    echo "Alter task started\n";
    $alter_result = alterTables($config->alter);
    echo $alter_result;
    echo "Alter task finished\n\n\n\n";
    
    // Create new table
    echo "Create task started\n";
    $create_result = createTables($config->new);
    echo $create_result;
    echo "Create task finished\n\n\n\n";

    // Add data to a table
    echo "Prefil data task started\n";
    $prefill_result = prefillTables($config->preFillData);
    echo $prefill_result;
    echo "Prefill task finished\n\n\n\n";

    /***************************************************
    FUNCTIONS
    ***************************************************/

    // Alter table
    function alterTables($tables) {
       global $c_db;
       $logs = '';
       foreach($tables as $table=>$action) {
           // Add columns
           foreach($action->add as $column=>$type) {
               $exist = isColumnExist($table, $column);

               if(!$exist){
                   $result = $c_db->query('ALTER TABLE '. $table .' ADD '. $column .' '. $type .' ', array());
                   if($result){
                       $logs .= "Alter Table : " . $table . " modified row: " . $column . " of type: " . $type . " \n";
                   } else {
                       $logs .= "Cannot Alter table :" . $table . "\n";
                   }
               } else {
                   $logs .= "ERROR!: Column \"" . $column . "\" already exist in table " . $table . " \n";
               }
           }
           // Drop colums
           foreach($action->drop as $column) {
               $exist = isColumnExist($table, $column);
               if($exist) {
                   $result = $c_db->query('ALTER TABLE '. $table .' DROP '. $column, array());
                   if($result){
                       $logs .= "Alter Table : " . $table . " droppped row: " . $column . " \n";
                   } else {
                       $logs .= "Cannot dropped column :" . $column . "\n";
                   }
               } else {
                   $logs .= "ERROR!: Column \"" . $column . "\" does not exist in table " . $table . " \n";
               }
           }
       }
        
        return $logs;
    }

    // Create table
    function createTables($tables) {
        global $c_db;
        $logs = '';
        foreach($tables as $table=>$columns) {
            $exists = isTableExist($table);
            if(!$exists) {
                $sqlFields = [];
                foreach($columns as $column=>$type) {
                    $sqlFields[] = $column . ' ' . $type;
                }
                $sqlStrings = join(',', $sqlFields);
                $result = $c_db->query('CREATE TABLE '. $table. ' ('. $sqlStrings .')', array());
                if($result) {
                    $logs .= "Created table: " . $table . "\n";
                } else {
                    $logs .= "ERROR!: " . $table . " could not be created.\n";
                }
            } else {
                $logs .= "ERROR!: " . $table . " already exist.\n";
            }
        }
        
        return $logs;
    }

    // Drop tables
    function dropTables($tables) {
        global $c_db;
        $logs = '';
        foreach($tables as $table){
            $exists = isTableExist($table);
            if($exists !== false) {
                $result = $c_db->query('DROP TABLE ' . $table);
                if($result){
                    $logs .= "Dropped Table :" . $table . "\n";
                }
            } else {
                $logs .= "ERROR!: Cannot Drop table :" . $table . "\n";
            }
        }
        
        return $logs;
    }

    // Add content to table
    function prefillTables($tables) {
        global $c_db;
        $logs = '';
        foreach ($tables as $table => $data) {
            foreach ($data as $entries) {
                $rows = [];
                $values = [];
                foreach($entries as $item=>$value) {
                    $rows[] = $item;
                    $values[] = $value;
                }
                $sqlRow = join(',', $rows);
                $sqlValues = join(',', $values);
                $result = $c_db->query("INSERT INTO ". $table ." (". $sqlRow .") VALUES (". $sqlValues .")", array());
                if($result) {
                    $logs .= "Added data to table: \"" . $table . "\" \n";
                } else {
                    $logs .= "ERROR!: Cannot add data to table: \"" . $table . "\" \n";
                }
            }
        }
    }

    // check if table exist
    function isTableExist($table_name) {
        global $c_db;
        $exist = $c_db->fetch_rows("
            SELECT *
            FROM information_schema.tables
            WHERE table_schema = ?
                AND table_name = ?
            LIMIT 1;",
            true,
            array(DB_NAME, $table_name)
        );

        return $exist;
    }

    // Check if column exists
    function isColumnExist($table_name, $column_name) {
        global $c_db;
        $exist = $c_db->fetch_rows("
            SELECT *
            FROM information_schema.COLUMNS
            WHERE
            TABLE_SCHEMA = ?
            AND TABLE_NAME = ?
            AND COLUMN_NAME = ?",
            true,
            array(DB_NAME, $table_name, $column_name)
        );

        return $exist;
    }
?>