# Migration script documnetation

The migration script is use 3 files. The main data base configuration, a json file representing the data and table to create or midify, and the business logic on migration.php

The script follows these steps:

1. Drop table
2. Alter existing tables
3. Create new tables
4. Add data to existing tables.

All of the above are specified in the JSON configuration file that is in the 'migration.config' folder abd label to a version of the site.

The JSON configuration file must have the following structure:

```JSON
{
	"new": {
        "table_name": {
            "column_name": "MYSQL_DATA_TYPE",
            "example": "INT(10) NOT NULL AUTO_INCREMENT PRIMARY KEY",
        },
    },
    "alter":{
        "table_name": {
			"add": {
				"column_name": "MYSQL_DATA_TYPE" 
			},
			"drop": [
				"column_name"
			]
		}
    },
    "drop": [
        "table_name"
    ],
	"preFillData": {
		"table_name": {
            "column_name":"DATA_TO_INPUT",
		}
	}
}
```

An init file is provided to generate the installation of the CMS.