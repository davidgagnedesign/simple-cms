<?php

include($_SERVER['DOCUMENT_ROOT'] . '/server/class/messages/Message.php');

$request = file_get_contents('php://input');
$req_data = json_decode($request);

$msg = New Message();

$saved = $msg->save_to_db($req_data);


header('Content-Type: application/json');
echo json_encode($saved);

?>