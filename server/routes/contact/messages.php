<?php
include($_SERVER['DOCUMENT_ROOT'] . '/server/class/messages/Message.php');

$msg = New Message();

if (!$_GET['id']) {
    $result = $msg->get_all_messages();
} else {
    $result = $msg->get_single_message($_GET['id']);
}
    
header('Content-Type: application/json');
echo json_encode($result);
?>