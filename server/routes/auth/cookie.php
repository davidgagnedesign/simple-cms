<?php

include($_SERVER['DOCUMENT_ROOT']  . '/server/config/config.php');
include($_SERVER['DOCUMENT_ROOT']  . '/server/lib/jwt.php');

$request = file_get_contents('php://input');
$req_data = json_decode($request);
$c_jwt = new Jwt;

$result = $c_jwt->decode($req_data->token, JWT_KEY);

header('Content-Type: application/json');
echo $result;
?>