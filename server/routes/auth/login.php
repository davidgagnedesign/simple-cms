<?php

include($_SERVER['DOCUMENT_ROOT']  . '/server/class/users/Auth.php');

$request = file_get_contents('php://input');
$req_data = json_decode($request);

$c_user = new UserAuth($req_data->email, $req_data->password);

$result = $c_user->login();

header('Content-Type: application/json');
echo json_encode($result);

?>