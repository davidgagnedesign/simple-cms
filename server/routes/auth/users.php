<?php
include($_SERVER['DOCUMENT_ROOT']  . '/server/class/users/Users.php');
include($_SERVER['DOCUMENT_ROOT'] . '/server/lib/jwt.php');

$c_users = new Users();
$result = $c_users->get_all_users();
$c_jwt = new Jwt;

$auth = $c_jwt->decode($_GET['token'], JWT_KEY);

header('Content-Type: application/json');
echo json_encode($result);

?>