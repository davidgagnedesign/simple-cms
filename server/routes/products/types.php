<?php

include ($_SERVER['DOCUMENT_ROOT'] . '/server/class/products/Types.php');
include($_SERVER['DOCUMENT_ROOT'] . '/server/lib/jwt.php');

$c_types = New Types();
$c_jwt = new Jwt;

$request = file_get_contents('php://input');
$req_data = json_decode($request);

if ($_SERVER['REQUEST_METHOD'] === 'GET') {
     $result = $c_types->getTypes();
}

if ($_SERVER['REQUEST_METHOD'] === 'PUT') {
    $auth = $c_jwt->decode($req_data->token, JWT_KEY); 
    $result = $c_types->createTypes($req_data->name);
}

if ($_SERVER['REQUEST_METHOD'] === 'POST' && $req_data->requestType === 'delete') {
    $auth = $c_jwt->decode($req_data->token, JWT_KEY); 
    $result = $c_types->deleteTypes($req_data->id);
}

if ($_SERVER['REQUEST_METHOD'] === 'POST' && $req_data->requestType === 'edit') {
    $auth = $c_jwt->decode($req_data->token, JWT_KEY); 
    $result = $c_types->editType($req_data);
}

header('Content-Type: application/json');
echo json_encode($result);

?>