<?php

include ($_SERVER['DOCUMENT_ROOT'] . '/server/class/products/Products.php');
include($_SERVER['DOCUMENT_ROOT'] . '/server/lib/jwt.php');

$request = file_get_contents('php://input');
$req_data = json_decode($request);

$c_products = New Products();
$c_jwt = new Jwt;

$auth = $c_jwt->decode($req_data->token, JWT_KEY);

$result = $c_products->createNew($req_data);

header('Content-Type: application/json');
echo json_encode($result);

?>