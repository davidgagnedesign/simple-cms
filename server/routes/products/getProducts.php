<?php
include ($_SERVER['DOCUMENT_ROOT'] . '/server/class/products/Products.php');

$c_products = New Products();

if ($_GET['limit']) {
    $result = $c_products->getProducts($_GET['limit']);
}

if ($_GET['id']) {
    $result = $c_products->getSingleProduct($_GET['id']);
}


header('Content-Type: application/json');
echo json_encode($result);

?>