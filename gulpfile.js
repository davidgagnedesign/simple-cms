var gulp = require('gulp');
var sass = require('gulp-sass');
var watch = require('gulp-watch');
var uglify = require("gulp-uglify");
var concat = require("gulp-concat");
var chmod = require('gulp-chmod');
var wait = require('gulp-wait');

// Scss to style.css compiled file
gulp.task('sass', function() {
    gulp.src('app/css/style.scss')
        .pipe(wait(2000))
        .pipe(sass())
        .pipe(chmod(755))
        .pipe(gulp.dest('build/css/'));
});

// Js compilation to main.js
gulp.task('compileJs', function() {
    gulp.src('app/**/*.js')
        .pipe(wait(2000))
        .pipe(concat('main.js'))
        .pipe(chmod(755))
        // .pipe(uglify())
        .pipe(gulp.dest('build/js'));
});

// Get desire node_module and compile to vendor.js
gulp.task('node_modules', function() {
    var modules = [
        'node_modules/trumbowyg/'
    ];
    // compile vendor js
    gulp.src(modules + '*.js')
        .pipe(wait(2000))
        .pipe(concat('vendor.js'))
        .pipe(chmod(755))
        .pipe(gulp.dest('build/js'));
    // compile vendor css
    gulp.src(modules + '*.css')
        .pipe(wait(2000))
        .pipe(concat('vendor.css'))
        .pipe(chmod(755))
        .pipe(gulp.dest('build/css'));
});

// Watch for changes
gulp.task('watch', function() {
    gulp.watch('app/**', ['sass', 'compileJs']);
});

//Default
gulp.task('default', ['sass', 'compileJs', 'node_modules', 'watch']);

// Compile for installation
gulp.task('install', ['sass', 'compileJs', 'node_modules']);