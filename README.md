# Simple php and angular 1.5 CMS

### Work in progress

A very simple CMS made with a PHP backend and a Angular 1.5 front-end. The goal was to have a very easy to use alternative to other popular CMS out there. With a totally separate front-end and back-end, to make it easily customizable.

A user can create products, products, categories, and manage contact via the admin page.

## Default user credentials

`User email = simpleCms@gmail.com`
`Password = 123456`

> These can be changed in the `init.json` file used in the migration script. The password is a SHA256 hash string. A create user interface is still to come.

## Setup


1- Create a LAMP stack
> I personnaly like to use a droplet on digitalocean.com with the one click app install

2- Log trought SSH to your newly created droplet 

3- Update ubuntu repo 
	``` sudo apt-get update ```

4- Install, git, nodejs, npm
	``` sudo apt-get install git nodejs npm -y ```

5- Install gulp globaly
	``` npm install gulp --global ```

6- Install gulp-cli globaly
	``` npm install gulp-cli --global ```

* For some reason the installation of gulp and gulp-cli seems to freeze on digital ocean just exit the installation by pressing CTRL+C and re-run the command

7- Make sure your apache mode rewrite is enabled
       ```sudo a2enmod rewrite```

8- Go to the default apache website repo 
	``` cd /var/www/ ```

9- Create a mysql database with a user that will be the app:
        ```mysql -u root -p``` 
        ```CREATE DATABASE simple_cms;```
        ```CREATE USER 'simpleCmsApp'@'localhost' IDENTIFIED BY 'Password123';```
        ```GRANT ALL PRIVILEGES ON * . * TO 'simpleCmsApp'@'localhost';```
        ```FLUSH PRIVILEGES;```

> Here you need to change the user name and password to something more secure. you will also need these credentials for a configuration file inside the app code

10- Clone the project from gitlab and make sure it’s in the `html` folder. You might have to delete the content of the html folder that is already present
	``` git clone http://path_of _the_project   html ```

11- Navigate to the project root that should be in html 
	``` cd html ```

12- Change owner and permission for the assets folder.
```chmod -r 775 build/images```
```chown -R root:www-data images/```

13- Install the application
	``` npm install ```
* If you get this error ‘’/usr/bin/env: node: No such file or directory” run the following command and then run gulp again:
``` ln -s /usr/bin/nodejs /usr/bin/node ```

14- Modify the config file to have access to database
```vim server/config/config.php```

15- Run the migration script and follow the instruction. 
``` php server/config/migration/migration.php```
> The first time you need to run the `init` script to create the proper table for the database. A `migration_doc.md` file is in the folder to explain the different configurations options. 


	
14- Start the watcher that compile css and js 
	``` gulp ```
