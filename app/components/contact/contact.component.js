(function() {

    function contactFromCtrl(ContactService) {
        var vm = this;
        vm.as_error = false;
        vm.sent = false;
        vm.sending = false;

        vm.sendMessage =  function() {
            vm.as_error = false;
            vm.invalid_email = false;
            vm.invalid_name = false;
            vm.invalid_message = false;
            vm.sending = true;

            if (!vm.email) { vm.invalid_email = true; vm.as_error = true; }
            if (!vm.name) { vm.invalid_name = true;  vm.as_error = true; }
            if (!vm.message) { vm.invalid_message = true; vm.as_error = true; }
            if (vm.as_error) return;

            var data = {
                email: vm.email,
                name: vm.name,
                message: vm.message
            };
            
            ContactService.sendMessage(data).then(function(response) {
                if (response) vm.sent = true;
                vm.sending = false;
            });
        };
    }

    angular.module('mainApp')
        .component('contact', {
            templateUrl: 'app/components/contact/contact.partial.html',
            controller: ['ContactService', contactFromCtrl]
        });
})();