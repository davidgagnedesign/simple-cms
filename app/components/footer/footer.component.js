(function(){
    angular.module('mainApp')
        .component('appFooter', {
            templateUrl: 'app/components/footer/footerPartial.html'
        });
})();