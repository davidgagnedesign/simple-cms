(function() {

    function loginCtrl(AuthService) {

        var vm = this;

        vm.processForm = function() {
            var data = {
                email: vm.email,
                password: vm.password
            }
            AuthService.login(data);
        };
    }

    angular.module('mainApp')
        .component('loginForm', {
            templateUrl: 'app/components/login/login.html',
            controller: loginCtrl
        });
    
})();
