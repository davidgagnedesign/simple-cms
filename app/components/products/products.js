(function() {

    function productsCtrl(ProductsService) {

        var vm = this;
        ProductsService.getProducts(10000).then(function(response) {
            vm.products = response;
        });
    }

    angular.module('mainApp')
        .controller('productsCtrl', ['ProductsService', productsCtrl]);
})();
