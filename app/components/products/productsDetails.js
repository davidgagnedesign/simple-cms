(function() {

    function productsDetailsCtrl($routeParams, $scope, $window, ProductsService) {
        ProductsService.getSingleProduct($routeParams.id).then(function(response) {
            $scope.product = response[0];
            $('.product_description').html($scope.product.description);
        });

        $scope.go_back = function() {
            $window.history.back();
        };
        
        $scope.switch_images = function(file) {
            $('#main_image').attr('src', file);
        };
    }

    angular.module('mainApp')
        .controller('productsDetailsCtrl', ['$routeParams', '$scope', '$window', 'ProductsService', productsDetailsCtrl]);
})();