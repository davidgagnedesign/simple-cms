(function() {

    function productCardsCtrl($routeParams, $location, ProductsService) {

        var vm = this;
        vm.goToDetails = function(id) {
            var path = '/' + ['product', id].join('/');
            $location.path(path);
        };
    }

    angular.module('mainApp')
        .component('productCards', {
            bindings: {
                product: '='
            },
            templateUrl: 'app/components/products/productsCards.html',
            controller: ['$routeParams', '$location', 'ProductsService', productCardsCtrl]
        });

})();