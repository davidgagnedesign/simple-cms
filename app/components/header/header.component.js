(function() {
    function headerCtrl($scope, $routeParams, $location) {
        var vm = this;
        vm.showMenu = function() {
            $('nav ul').toggle({
                duratin: 400,
                easing: 'linear'
            });
        }
        

        $scope.$on('$routeChangeSuccess', function(next, current) { 
            var menuItems = $('.header_links');
            for (var i = 0; i <= menuItems.length; i++) {
                var path = $(menuItems[i]).attr('href');
                if (path === $location.path()) {
                    $(menuItems[i]).addClass('is-active');
                } else {
                    $(menuItems[i]).removeClass('is-active');
                }
            }
        });

        $(window).resize(function() {
            var displayMenu;
            ($(window).width() > 425) ? displayMenu = 'block': displayMenu = 'none';
            $('nav ul').css({
                display: displayMenu
            });
        });
    }

    angular.module('mainApp')
        .component('appHeader', {
            templateUrl: 'app/components/header/headerPartial.html',
            controller: ['$scope', '$routeParams', '$location', headerCtrl]
        });
})();