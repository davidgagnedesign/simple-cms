(function() {
    
    function adminUsersCtrl($location, AuthService) {
        var vm = this;
        
        AuthService.getAllUsers().then(function(response) {
            console.log('Users components =>', response);
            vm.users = response;
        });
        
    }
    
    angular.module('mainApp')
    .component('adminUsers', {
        bindings: {},
        templateUrl: 'app/components/admin/users/users.partial.html',
        controller: ['$location', 'AuthService', adminUsersCtrl]
    });
})();