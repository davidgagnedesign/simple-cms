(function() {
    
    function adminProductManagerController($routeParams, $location, $scope, ProductsService) {
        var vm = this;
        var imageCropper = $('#image_cropper');
        vm.ProductsService = ProductsService;
        vm.mode = 'create';
        vm.croppedImg;
        vm.errors = {};
		vm.productsImages = [];
        vm.imageTool = true;

        if ($routeParams.id !== 'new') {
            vm.mode = 'edit';
            vm.imageTool = false;
            vm.ProductsService.getSingleProduct($routeParams.id).then(function(response) {
				vm.productDetails = response[0];
				vm.productsImages = vm.productDetails.images;
				console.log('vm.productDetails', vm.productDetails);
				$('#productDescription').trumbowyg('html', vm.productDetails.description);
            });
        }

		vm.ProductsService.getProductTypes().then(function(response) {
            vm.types = response;
        });
        
        angular.element($('#image_input').change(function() {
            read_image(this.files[0]);
        }));
		
		angular.element($('#productDescription').trumbowyg());

        vm.submitProduct = function(is_valid) {
			if (!is_valid) return;
            if (!vm.productsImages.length && vm.mode === 'create') return;
			vm.productDetails.description = $('#productDescription').trumbowyg('html');
            vm.productDetails.productsImages = vm.productsImages;
            vm.ProductsService.saveProduct(vm.productDetails).then(function(response) {
				if (response) alert('Products saved !');
				// if (response) { $location.path('/admin'); }
            });
        };
		
        vm.updateProduct = function(is_valid) {
            if (!is_valid) return;
            vm.productDetails.productsImages = vm.productsImages;
			vm.productDetails.description = $('#productDescription').trumbowyg('html');
            vm.ProductsService.updateProduct(vm.productDetails).then(function(response) {
				if (response) alert('Products updated !');
            });
        };
        
        vm.deleteProduct = function(id) {
            var answer = confirm('Are you sure you want to delete this entry ?');
            if (answer) {
                var data = {
                    id: id
                };
                vm.ProductsService.deleteProduct(data).then(function(response) {
                    response === 1 ? $location.path('/admin') : '';
                });
            }
        };
				
		vm.selectMainImage = function(event, index) {
			var container = event.target.parentNode;
			$('.main_product').removeClass('main_product');
			$(container).addClass('main_product');
			assignMainImage(index);
		};
		
		vm.deleteImages = function(index) {
			vm.productsImages.splice(index, 1);
		};
		
		function assignMainImage(index) {
			for(var i = 0; i < vm.productsImages.length; i++) {
				i === index ? vm.productsImages[i].isMain = true : vm.productsImages[i].isMain = false;
			}
		}
		
		function read_image(file) {
            var fileReader = new FileReader();
        	fileReader.onload = function(fileLoadedEvent) {
				var image = {
					file: fileLoadedEvent.target.result,
					isMain: vm.productsImages.length === 0
				};
				vm.productsImages.push(image);
				$scope.$apply();
			};
        	fileReader.readAsDataURL(file);
        }
		
    }
    
    angular.module('mainApp')
    .component('adminProductManager', {
        templateUrl: 'app/components/admin/products/manager.partial.html',
        controller: ['$routeParams', '$location', '$scope', 'ProductsService', adminProductManagerController]
    });
})();