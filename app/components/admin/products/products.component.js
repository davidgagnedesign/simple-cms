(function() {

    function adminProductsController($location, ProductsService) {
        var vm = this;
        ProductsService.getProducts(1000000).then(function(response) {
            vm.products = response;
        });
        
        vm.goToManager = function(id) {
            var path = 'admin/product/' + id;
            $location.path(path); 
        };
        
    }

    angular.module('mainApp')
    .component('adminProducts', {
        templateUrl: '/app/components/admin/products/products.partial.html',
        controller: ['$location', 'ProductsService', adminProductsController]
    });
})();