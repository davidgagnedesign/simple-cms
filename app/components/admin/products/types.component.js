(function(){
    
    function typesController($window, ProductsService) {
        vm = this;
        vm.showCreate = false;
        vm.createName = '';
        vm.editMode = 0;
        
        ProductsService.getProductTypes().then(function(response) {
            vm.types = response;
        });
        
        vm.toggleCreate = function() {
            vm.showCreate = true;
        };
        
        vm.saveType = function() {
            ProductsService.createType(vm.createName).then(function(response) {
                response === 1 ? $window.location.reload() : '';
            });
        };
        
        vm.deleteType = function(id) {
            var answer = confirm('Are you sure you want to delete this entry ?');
            if (answer) {
                ProductsService.deleteType(id).then(function(response) {
                    response === 1 ? $window.location.reload() : '';
                });
            }
        };

        vm.edit = function(id, name) {
            var data = {
                id: id,
                name: name
            };
            ProductsService.editType(data).then(function(response) {
                response === 1 ? $window.location.reload() : '';
            });
        }

        vm.editor_toggle = function(id) {
            vm.editMode = id;
        };
    }
    
    angular.module('mainApp')
    .component('typesManager', {
        templateUrl: '/app/components/admin/products/types.partial.html',
        controller: ['$window', 'ProductsService', typesController]
    });
})();