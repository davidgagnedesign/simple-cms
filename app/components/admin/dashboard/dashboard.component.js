(function() {
    
    function adminDashboardController() {
        var vm = this;
    }
    
    angular.module('mainApp')
        .component('adminDashboard', {
            templateUrl: 'app/components/admin/dashboard/dashboard.partial.html',
            controller: adminDashboardController
        });
    
})();