(function() {
    
    function adminMessagesCtrl($location, ContactService) {
        var vm = this;
        
        ContactService.getMessages().then(function(response) {
            vm.messages = response;
        });
        
        vm.goToEditor = function(id) {
            var path = 'admin/message/' + id;
            $location.path(path); 
        };
    }
    
    angular.module('mainApp')
    .component('adminMessages', {
        bindings: {},
        templateUrl: 'app/components/admin/messages/messages.partial.html',
        controller: ['$location', 'ContactService', adminMessagesCtrl]
    });
})();