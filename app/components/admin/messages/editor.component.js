(function() {
    
    function messageEditorCtrl($routeParams, ContactService) {
        var vm = this;

        ContactService.getSingleMessage($routeParams.id).then(function(response) {
            vm.messageData = response;
            vm.messageParams = JSON.parse(vm.messageData.messageParams);
            console.log('vm.messageParams', vm.messageParams);
        });
    }
    
    angular.module('mainApp')
    .component('messageEditor', {
        bindings: {},
        templateUrl: 'app/components/admin/messages/editor.partial.html',
        controller: ['$routeParams', 'ContactService', messageEditorCtrl]
    });
})();