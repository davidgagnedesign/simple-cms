(function(){
	function adminRouter($routeProvider, $locationProvider ) {
		$routeProvider
		.when('/admin', {
			template: '<admin-dashboard></admin-dashboard>',
			resolve: {
				auth: ['AuthService', function(AuthService) {
					var auth = AuthService.authenticate();
				}]
			}
		})
		.when('/admin/product/:id', {
			template: '<admin-product-manager></admin-product-manager>',
			resolve: {
				auth: ['AuthService', function(AuthService) {
					var auth = AuthService.authenticate();
				}]
			}
		})
		.when('/admin/message/:id', {
			template: '<message-editor></message-editor>',
			resolve: {
				auth: ['AuthService', function(AuthService) {
					var auth = AuthService.authenticate();
				}]
			}
		})
        
		$locationProvider.html5Mode(true);
	}
	
	angular.module('mainApp')
		.config([
			'$routeProvider',
			'$locationProvider',
			adminRouter]);
})();