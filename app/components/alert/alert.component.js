(function(){
    
    function alertController($rootScope, $timeout) {
        vm = this;
        vm.asAlert = false;
 
        $rootScope.$on('event:alert', function(event, args) {
            vm.message = args.params.message;
            vm.type = args.params.type;
            vm.timeout = args.params.timeout || 0;
            vm.persistent = args.params.persistent || false;
            vm.asAlert = true;
            
            if (vm.timeout > 0 && !vm.persistent) {
                $timeout(function() {
                    vm.asAlert = false;
                }, vm.timeout);
            }
        });

        $rootScope.$on('$routeChangeStart', function() {
            vm.asAlert = vm.persistent ? true : false;
        });

        vm.closeAlert = function() {
            vm.asAlert = false;
            vm.persistent = false;
        };
    }
    
    angular.module('mainApp')
        .component('alert', {
            templateUrl: '/app/components/alert/alertPartial.html',
            controller: alertController,
            controllerAs: 'alert'
        })
})();