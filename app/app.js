(function() {

    function appRouter($routeProvider, $locationProvider) {

        $routeProvider
            .when('/', {
                templateUrl: 'app/components/home/home.html'
            })
            .when('/about', {
                templateUrl: 'app/components/about/about.html'
            })
            .when('/login', {
                template: '<login-form></login-form>',
            })
            .when('/contact', {
                template: '<contact></contact>'
            })
            .when('/products', {
                templateUrl: 'app/components/products/products.html',
                controller: 'productsCtrl',
                controllerAs: 'products'
            })
            .when('/product/:id', {
                templateUrl: 'app/components/products/productsDetails.html',
                controller: 'productsDetailsCtrl'
            })

        //***************************
        // Default
        //***************************
        .otherwise({
            templateUrl: 'app/components/404/404.html'
        })

        $locationProvider.html5Mode(true);

    }

    angular.module('mainApp', [
            'ngRoute',
            'ngCookies'
        ])
        .config([
            '$routeProvider',
            '$locationProvider',
            appRouter
        ]);
})();