(function(){
    function AuthService($http, $q, $location, AlertService, CookieService) {

        var authenticate = function() {
            CookieService.checkAuthCookie().then(function(response) {
                if (response === 'Invalid token' || response.type !== 1) {
                    $location.path('/login');
                    AlertService.set({
                        type: 'warning',
                        message: 'Your session is expired. Please login again.',
                        timeout: 4000,
                        persistent: true
                    });
                }
            });
        };
 
        // Login user
        var login = function(data) {

            this.userApi = '/server/routes/auth/login.php';

            $http.post(this.userApi, data).then(
                function(response) {
                    if(response.data === false) {
                        AlertService.set({
                            type: 'error',
                            message: 'The login informations are not correct',
                            timeout: 4000,
                            persistent: false
                        });
                        return;
                    } else {
                        var cookieParams = {
                            name: 'auth',
                            token: response.data.AuthJwt,
                        };
                        CookieService.putCookie(cookieParams);
                        $location.path('/admin');
                    }
                },
                function(error) {
                    console.log('error', error);
                }
            );
        };
        
        var logout = function() {
            CookieService.remove('auth');
        };
        
        var getAllUsers = function() {
            var deferred = $q.defer();
            var token = CookieService.getCookie('auth');
            var url = '/server/routes/auth/users.php?token=' + token;
            $http.get(url).then(function(response) {
                deferred.resolve(response.data);
            });

            return deferred.promise;
        };

        return {
            authenticate: authenticate,
            login: login,
            logout: logout,
            getAllUsers: getAllUsers
        };
    }

    angular.module('mainApp')
    .factory('AuthService', ['$http', '$q', '$location', 'AlertService', 'CookieService', AuthService]);
   
})();