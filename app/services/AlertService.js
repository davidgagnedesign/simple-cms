(function() {
    'use strict';
    
    function AlertService($rootScope) {

        var set = function(params) {
            $rootScope.$broadcast('event:alert', {params: params});
        };
        
        return {
            set: set
        };
    }
    
    angular.module('mainApp')
        .factory('AlertService', ['$rootScope', AlertService]);
})();