(function() {
	'use strict';

	function CookieService($cookies, $http, $q) {
		
		var putCookie = function(params) {
			$cookies.put(params.name, params.token);
		};
		
		var getCookie = function(name) {
			return $cookies.get(name);
		};
		
		var checkAuthCookie = function() {
			var deferred = $q.defer();
			var cookie = this.getCookie('auth');
			var data = {
				token: cookie
			}
			$http.post('server/routes/auth/cookie.php', data)
				.success(function(response) {
	                deferred.resolve(response);
	            }).error(function(data, status, headers, config) {
	                deferred.reject(data);
	            });
            return deferred.promise;
		};

		var remove = function(name) {
			$cookies.remove(name);
		};
		

		return {
			putCookie: putCookie,
			getCookie: getCookie,
			checkAuthCookie: checkAuthCookie,
			remove: remove
		};
	}

	angular.module('mainApp')
    .factory('CookieService', ['$cookies', '$http', '$q', CookieService]);
})();