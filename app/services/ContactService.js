(function() {

    function ContactService ($http, $q) {
        var ContactFactory = {};

        ContactFactory.sendMessage = function(data) {
            var deferred = $q.defer();
            var url = '/server/routes/contact/contactForm.php';
            $http.post(url, data).then(function(response) {
                console.log('response from service =>', response);
                if (response.data === 1) deferred.resolve(true);
            });
            return deferred.promise;
        };
        
        ContactFactory.getMessages = function() {
            var deferred = $q.defer();
            var url = '/server/routes/contact/messages.php';
            $http.get(url).then(function(response) {
                deferred.resolve(response.data);
            });

            return deferred.promise;
        };
        
        ContactFactory.getSingleMessage = function(id) {
            var deferred = $q.defer();   
            var url = '/server/routes/contact/messages.php?id=' + id;
            $http.get(url).then(function(response) {
                deferred.resolve(response.data);
            });

            return deferred.promise;
        };

        return ContactFactory;
    }

    angular.module('mainApp')
    .factory('ContactService', ['$http', '$q', ContactService]);
})();
