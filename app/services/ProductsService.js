(function() {
    function ProductsService($q, $http, CookieService) {

        var productsFactory = {};

        productsFactory.getProducts = function(limit) {
            var deffered = $q.defer();
            var query = 'server/routes/products/getProducts.php?limit=' + limit;
            $http.get(query)
                .success(function(response) {
                deffered.resolve(response);
                })
                .error(function(error) {
                    deffered.reject(error);
                });
            return deffered.promise;
        }

        productsFactory.getSingleProduct = function(id) {
            var deffered = $q.defer();
            var query = 'server/routes/products/getProducts.php';
            var params = {
                id: id
            };
            $http({
                    url: query,
                    method: 'GET',
                    params: params
                })
                .success(function(response) {
                    deffered.resolve(response);
                })
                .error(function(error) {
                    deffered.reject(error);
                });
            return deffered.promise;
        };

        productsFactory.saveProduct = function(data) {
            var deffered = $q.defer();
            var query = 'server/routes/products/new.php';
            data.token = CookieService.getCookie('auth');
            $http.put(query, data)
                .success(function(response) {
                    deffered.resolve(response);
                })
                .error(function(error) {
                    deffered.reject(error);
                });
            return deffered.promise;
        };

        productsFactory.deleteProduct = function(data) {
            var deffered = $q.defer();
            var query = 'server/routes/products/delete.php';
            data.token = CookieService.getCookie('auth');
            $http.post(query, data)
                .success(function(response) {
                    deffered.resolve(response);
                })
                .error(function(error) {
                    deffered.reject(error);
                });
            return deffered.promise;
        };

        productsFactory.updateProduct = function(data) {
            var deffered = $q.defer();
            var query = 'server/routes/products/update.php';
            data.token = CookieService.getCookie('auth');
            $http.post(query, data)
                .success(function(response) {
                    deffered.resolve(response);
                })
                .error(function(error) {
                    deffered.reject(error);
                });
            return deffered.promise;
        };

        productsFactory.getProductTypes = function() {
            var deffered = $q.defer();
            var query = 'server/routes/products/types.php';
            $http({
                    url: query,
                    method: 'GET',
                })
                .success(function(response) {
                    deffered.resolve(response);
                })
                .error(function(error) {
                    deffered.reject(error);
                });
            return deffered.promise;
        };

        productsFactory.createType = function(name) {
            var deffered = $q.defer();
            var query = 'server/routes/products/types.php';
            var data = {};
            data.name = name;
            data.token = CookieService.getCookie('auth');
            $http.put(query, data)
                .success(function(response) {
                    deffered.resolve(response);
                })
                .error(function(error) {
                    deffered.reject(error);
                });
            return deffered.promise;
        };

        productsFactory.deleteType = function(id) {
            var deffered = $q.defer();
            var query = 'server/routes/products/types.php';
            var data = {};
            data.id = id;
            data.requestType = 'delete';
            data.token = CookieService.getCookie('auth');
            $http.post(query, data)
                .success(function(response) {
                    deffered.resolve(response);
                })
                .error(function(error) {
                    deffered.reject(error);
                });
            return deffered.promise;
        };

        productsFactory.editType = function(data) {
            var deffered = $q.defer();
            var query = 'server/routes/products/types.php';
            data.requestType = 'edit';
            data.token = CookieService.getCookie('auth');
            $http.post(query, data)
                .success(function(response) {
                    deffered.resolve(response);
                })
                .error(function(error) {
                    deffered.reject(error);
                });
            return deffered.promise;
        };


        return productsFactory;
    }

    angular.module('mainApp')
        .factory('ProductsService', ['$q', '$http', 'CookieService', ProductsService])
})();