(function() {

    function appRouter($routeProvider, $locationProvider) {

        $routeProvider
            .when('/', {
                templateUrl: 'app/components/home/home.html'
            })
            .when('/about', {
                templateUrl: 'app/components/about/about.html'
            })
            .when('/login', {
                template: '<login-form></login-form>',
            })
            .when('/contact', {
                template: '<contact></contact>'
            })
            .when('/products', {
                templateUrl: 'app/components/products/products.html',
                controller: 'productsCtrl',
                controllerAs: 'products'
            })
            .when('/product/:id', {
                templateUrl: 'app/components/products/productsDetails.html',
                controller: 'productsDetailsCtrl'
            })

        //***************************
        // Default
        //***************************
        .otherwise({
            templateUrl: 'app/components/404/404.html'
        })

        $locationProvider.html5Mode(true);

    }

    angular.module('mainApp', [
            'ngRoute',
            'ngCookies'
        ])
        .config([
            '$routeProvider',
            '$locationProvider',
            appRouter
        ]);
})();
(function() {
    'use strict';
    
    function AlertService($rootScope) {

        var set = function(params) {
            $rootScope.$broadcast('event:alert', {params: params});
        };
        
        return {
            set: set
        };
    }
    
    angular.module('mainApp')
        .factory('AlertService', ['$rootScope', AlertService]);
})();
(function(){
    function AuthService($http, $q, $location, AlertService, CookieService) {

        var authenticate = function() {
            CookieService.checkAuthCookie().then(function(response) {
                if (response === 'Invalid token' || response.type !== 1) {
                    $location.path('/login');
                    AlertService.set({
                        type: 'warning',
                        message: 'Your session is expired. Please login again.',
                        timeout: 4000,
                        persistent: true
                    });
                }
            });
        };
 
        // Login user
        var login = function(data) {

            this.userApi = '/server/routes/auth/login.php';

            $http.post(this.userApi, data).then(
                function(response) {
                    if(response.data === false) {
                        AlertService.set({
                            type: 'error',
                            message: 'The login informations are not correct',
                            timeout: 4000,
                            persistent: false
                        });
                        return;
                    } else {
                        var cookieParams = {
                            name: 'auth',
                            token: response.data.AuthJwt,
                        };
                        CookieService.putCookie(cookieParams);
                        $location.path('/admin');
                    }
                },
                function(error) {
                    console.log('error', error);
                }
            );
        };
        
        var logout = function() {
            CookieService.remove('auth');
        };
        
        var getAllUsers = function() {
            var deferred = $q.defer();
            var token = CookieService.getCookie('auth');
            var url = '/server/routes/auth/users.php?token=' + token;
            $http.get(url).then(function(response) {
                deferred.resolve(response.data);
            });

            return deferred.promise;
        };

        return {
            authenticate: authenticate,
            login: login,
            logout: logout,
            getAllUsers: getAllUsers
        };
    }

    angular.module('mainApp')
    .factory('AuthService', ['$http', '$q', '$location', 'AlertService', 'CookieService', AuthService]);
   
})();
(function() {

    function ContactService ($http, $q) {
        var ContactFactory = {};

        ContactFactory.sendMessage = function(data) {
            var deferred = $q.defer();
            var url = '/server/routes/contact/contactForm.php';
            $http.post(url, data).then(function(response) {
                console.log('response from service =>', response);
                if (response.data === 1) deferred.resolve(true);
            });
            return deferred.promise;
        };
        
        ContactFactory.getMessages = function() {
            var deferred = $q.defer();
            var url = '/server/routes/contact/messages.php';
            $http.get(url).then(function(response) {
                deferred.resolve(response.data);
            });

            return deferred.promise;
        };
        
        ContactFactory.getSingleMessage = function(id) {
            var deferred = $q.defer();   
            var url = '/server/routes/contact/messages.php?id=' + id;
            $http.get(url).then(function(response) {
                deferred.resolve(response.data);
            });

            return deferred.promise;
        };

        return ContactFactory;
    }

    angular.module('mainApp')
    .factory('ContactService', ['$http', '$q', ContactService]);
})();

(function() {
	'use strict';

	function CookieService($cookies, $http, $q) {
		
		var putCookie = function(params) {
			$cookies.put(params.name, params.token);
		};
		
		var getCookie = function(name) {
			return $cookies.get(name);
		};
		
		var checkAuthCookie = function() {
			var deferred = $q.defer();
			var cookie = this.getCookie('auth');
			var data = {
				token: cookie
			}
			$http.post('server/routes/auth/cookie.php', data)
				.success(function(response) {
	                deferred.resolve(response);
	            }).error(function(data, status, headers, config) {
	                deferred.reject(data);
	            });
            return deferred.promise;
		};

		var remove = function(name) {
			$cookies.remove(name);
		};
		

		return {
			putCookie: putCookie,
			getCookie: getCookie,
			checkAuthCookie: checkAuthCookie,
			remove: remove
		};
	}

	angular.module('mainApp')
    .factory('CookieService', ['$cookies', '$http', '$q', CookieService]);
})();
(function() {
    function ProductsService($q, $http, CookieService) {

        var productsFactory = {};

        productsFactory.getProducts = function(limit) {
            var deffered = $q.defer();
            var query = 'server/routes/products/getProducts.php?limit=' + limit;
            $http.get(query)
                .success(function(response) {
                deffered.resolve(response);
                })
                .error(function(error) {
                    deffered.reject(error);
                });
            return deffered.promise;
        }

        productsFactory.getSingleProduct = function(id) {
            var deffered = $q.defer();
            var query = 'server/routes/products/getProducts.php';
            var params = {
                id: id
            };
            $http({
                    url: query,
                    method: 'GET',
                    params: params
                })
                .success(function(response) {
                    deffered.resolve(response);
                })
                .error(function(error) {
                    deffered.reject(error);
                });
            return deffered.promise;
        };

        productsFactory.saveProduct = function(data) {
            var deffered = $q.defer();
            var query = 'server/routes/products/new.php';
            data.token = CookieService.getCookie('auth');
            $http.put(query, data)
                .success(function(response) {
                    deffered.resolve(response);
                })
                .error(function(error) {
                    deffered.reject(error);
                });
            return deffered.promise;
        };

        productsFactory.deleteProduct = function(data) {
            var deffered = $q.defer();
            var query = 'server/routes/products/delete.php';
            data.token = CookieService.getCookie('auth');
            $http.post(query, data)
                .success(function(response) {
                    deffered.resolve(response);
                })
                .error(function(error) {
                    deffered.reject(error);
                });
            return deffered.promise;
        };

        productsFactory.updateProduct = function(data) {
            var deffered = $q.defer();
            var query = 'server/routes/products/update.php';
            data.token = CookieService.getCookie('auth');
            $http.post(query, data)
                .success(function(response) {
                    deffered.resolve(response);
                })
                .error(function(error) {
                    deffered.reject(error);
                });
            return deffered.promise;
        };

        productsFactory.getProductTypes = function() {
            var deffered = $q.defer();
            var query = 'server/routes/products/types.php';
            $http({
                    url: query,
                    method: 'GET',
                })
                .success(function(response) {
                    deffered.resolve(response);
                })
                .error(function(error) {
                    deffered.reject(error);
                });
            return deffered.promise;
        };

        productsFactory.createType = function(name) {
            var deffered = $q.defer();
            var query = 'server/routes/products/types.php';
            var data = {};
            data.name = name;
            data.token = CookieService.getCookie('auth');
            $http.put(query, data)
                .success(function(response) {
                    deffered.resolve(response);
                })
                .error(function(error) {
                    deffered.reject(error);
                });
            return deffered.promise;
        };

        productsFactory.deleteType = function(id) {
            var deffered = $q.defer();
            var query = 'server/routes/products/types.php';
            var data = {};
            data.id = id;
            data.requestType = 'delete';
            data.token = CookieService.getCookie('auth');
            $http.post(query, data)
                .success(function(response) {
                    deffered.resolve(response);
                })
                .error(function(error) {
                    deffered.reject(error);
                });
            return deffered.promise;
        };

        productsFactory.editType = function(data) {
            var deffered = $q.defer();
            var query = 'server/routes/products/types.php';
            data.requestType = 'edit';
            data.token = CookieService.getCookie('auth');
            $http.post(query, data)
                .success(function(response) {
                    deffered.resolve(response);
                })
                .error(function(error) {
                    deffered.reject(error);
                });
            return deffered.promise;
        };


        return productsFactory;
    }

    angular.module('mainApp')
        .factory('ProductsService', ['$q', '$http', 'CookieService', ProductsService])
})();
(function(){
	function adminRouter($routeProvider, $locationProvider ) {
		$routeProvider
		.when('/admin', {
			template: '<admin-dashboard></admin-dashboard>',
			resolve: {
				auth: ['AuthService', function(AuthService) {
					var auth = AuthService.authenticate();
				}]
			}
		})
		.when('/admin/product/:id', {
			template: '<admin-product-manager></admin-product-manager>',
			resolve: {
				auth: ['AuthService', function(AuthService) {
					var auth = AuthService.authenticate();
				}]
			}
		})
		.when('/admin/message/:id', {
			template: '<message-editor></message-editor>',
			resolve: {
				auth: ['AuthService', function(AuthService) {
					var auth = AuthService.authenticate();
				}]
			}
		})
        
		$locationProvider.html5Mode(true);
	}
	
	angular.module('mainApp')
		.config([
			'$routeProvider',
			'$locationProvider',
			adminRouter]);
})();
(function(){
    
    function alertController($rootScope, $timeout) {
        vm = this;
        vm.asAlert = false;
 
        $rootScope.$on('event:alert', function(event, args) {
            vm.message = args.params.message;
            vm.type = args.params.type;
            vm.timeout = args.params.timeout || 0;
            vm.persistent = args.params.persistent || false;
            vm.asAlert = true;
            
            if (vm.timeout > 0 && !vm.persistent) {
                $timeout(function() {
                    vm.asAlert = false;
                }, vm.timeout);
            }
        });

        $rootScope.$on('$routeChangeStart', function() {
            vm.asAlert = vm.persistent ? true : false;
        });

        vm.closeAlert = function() {
            vm.asAlert = false;
            vm.persistent = false;
        };
    }
    
    angular.module('mainApp')
        .component('alert', {
            templateUrl: '/app/components/alert/alertPartial.html',
            controller: alertController,
            controllerAs: 'alert'
        })
})();
(function() {

    function contactFromCtrl(ContactService) {
        var vm = this;
        vm.as_error = false;
        vm.sent = false;
        vm.sending = false;

        vm.sendMessage =  function() {
            vm.as_error = false;
            vm.invalid_email = false;
            vm.invalid_name = false;
            vm.invalid_message = false;
            vm.sending = true;

            if (!vm.email) { vm.invalid_email = true; vm.as_error = true; }
            if (!vm.name) { vm.invalid_name = true;  vm.as_error = true; }
            if (!vm.message) { vm.invalid_message = true; vm.as_error = true; }
            if (vm.as_error) return;

            var data = {
                email: vm.email,
                name: vm.name,
                message: vm.message
            };
            
            ContactService.sendMessage(data).then(function(response) {
                if (response) vm.sent = true;
                vm.sending = false;
            });
        };
    }

    angular.module('mainApp')
        .component('contact', {
            templateUrl: 'app/components/contact/contact.partial.html',
            controller: ['ContactService', contactFromCtrl]
        });
})();
(function(){
    angular.module('mainApp')
        .component('appFooter', {
            templateUrl: 'app/components/footer/footerPartial.html'
        });
})();
(function() {
    function headerCtrl($scope, $routeParams, $location) {
        var vm = this;
        vm.showMenu = function() {
            $('nav ul').toggle({
                duratin: 400,
                easing: 'linear'
            });
        }
        

        $scope.$on('$routeChangeSuccess', function(next, current) { 
            var menuItems = $('.header_links');
            for (var i = 0; i <= menuItems.length; i++) {
                var path = $(menuItems[i]).attr('href');
                if (path === $location.path()) {
                    $(menuItems[i]).addClass('is-active');
                } else {
                    $(menuItems[i]).removeClass('is-active');
                }
            }
        });

        $(window).resize(function() {
            var displayMenu;
            ($(window).width() > 425) ? displayMenu = 'block': displayMenu = 'none';
            $('nav ul').css({
                display: displayMenu
            });
        });
    }

    angular.module('mainApp')
        .component('appHeader', {
            templateUrl: 'app/components/header/headerPartial.html',
            controller: ['$scope', '$routeParams', '$location', headerCtrl]
        });
})();
(function() {

    function loginCtrl(AuthService) {

        var vm = this;

        vm.processForm = function() {
            var data = {
                email: vm.email,
                password: vm.password
            }
            AuthService.login(data);
        };
    }

    angular.module('mainApp')
        .component('loginForm', {
            templateUrl: 'app/components/login/login.html',
            controller: loginCtrl
        });
    
})();

(function() {

    function productsCtrl(ProductsService) {

        var vm = this;
        ProductsService.getProducts(10000).then(function(response) {
            vm.products = response;
        });
    }

    angular.module('mainApp')
        .controller('productsCtrl', ['ProductsService', productsCtrl]);
})();

(function() {

    function productCardsCtrl($routeParams, $location, ProductsService) {

        var vm = this;
        vm.goToDetails = function(id) {
            var path = '/' + ['product', id].join('/');
            $location.path(path);
        };
    }

    angular.module('mainApp')
        .component('productCards', {
            bindings: {
                product: '='
            },
            templateUrl: 'app/components/products/productsCards.html',
            controller: ['$routeParams', '$location', 'ProductsService', productCardsCtrl]
        });

})();
(function() {

    function productsDetailsCtrl($routeParams, $scope, $window, ProductsService) {
        ProductsService.getSingleProduct($routeParams.id).then(function(response) {
            $scope.product = response[0];
            $('.product_description').html($scope.product.description);
        });

        $scope.go_back = function() {
            $window.history.back();
        };
        
        $scope.switch_images = function(file) {
            $('#main_image').attr('src', file);
        };
    }

    angular.module('mainApp')
        .controller('productsDetailsCtrl', ['$routeParams', '$scope', '$window', 'ProductsService', productsDetailsCtrl]);
})();
(function() {
    
    function adminDashboardController() {
        var vm = this;
    }
    
    angular.module('mainApp')
        .component('adminDashboard', {
            templateUrl: 'app/components/admin/dashboard/dashboard.partial.html',
            controller: adminDashboardController
        });
    
})();
(function() {
    
    function messageEditorCtrl($routeParams, ContactService) {
        var vm = this;

        ContactService.getSingleMessage($routeParams.id).then(function(response) {
            vm.messageData = response;
            vm.messageParams = JSON.parse(vm.messageData.messageParams);
            console.log('vm.messageParams', vm.messageParams);
        });
    }
    
    angular.module('mainApp')
    .component('messageEditor', {
        bindings: {},
        templateUrl: 'app/components/admin/messages/editor.partial.html',
        controller: ['$routeParams', 'ContactService', messageEditorCtrl]
    });
})();
(function() {
    
    function adminMessagesCtrl($location, ContactService) {
        var vm = this;
        
        ContactService.getMessages().then(function(response) {
            vm.messages = response;
        });
        
        vm.goToEditor = function(id) {
            var path = 'admin/message/' + id;
            $location.path(path); 
        };
    }
    
    angular.module('mainApp')
    .component('adminMessages', {
        bindings: {},
        templateUrl: 'app/components/admin/messages/messages.partial.html',
        controller: ['$location', 'ContactService', adminMessagesCtrl]
    });
})();
(function() {
    
    function adminProductManagerController($routeParams, $location, $scope, ProductsService) {
        var vm = this;
        var imageCropper = $('#image_cropper');
        vm.ProductsService = ProductsService;
        vm.mode = 'create';
        vm.croppedImg;
        vm.errors = {};
		vm.productsImages = [];
        vm.imageTool = true;

        if ($routeParams.id !== 'new') {
            vm.mode = 'edit';
            vm.imageTool = false;
            vm.ProductsService.getSingleProduct($routeParams.id).then(function(response) {
				vm.productDetails = response[0];
				vm.productsImages = vm.productDetails.images;
				console.log('vm.productDetails', vm.productDetails);
				$('#productDescription').trumbowyg('html', vm.productDetails.description);
            });
        }

		vm.ProductsService.getProductTypes().then(function(response) {
            vm.types = response;
        });
        
        angular.element($('#image_input').change(function() {
            read_image(this.files[0]);
        }));
		
		angular.element($('#productDescription').trumbowyg());

        vm.submitProduct = function(is_valid) {
			if (!is_valid) return;
            if (!vm.productsImages.length && vm.mode === 'create') return;
			vm.productDetails.description = $('#productDescription').trumbowyg('html');
            vm.productDetails.productsImages = vm.productsImages;
            vm.ProductsService.saveProduct(vm.productDetails).then(function(response) {
				if (response) alert('Products saved !');
				// if (response) { $location.path('/admin'); }
            });
        };
		
        vm.updateProduct = function(is_valid) {
            if (!is_valid) return;
            vm.productDetails.productsImages = vm.productsImages;
			vm.productDetails.description = $('#productDescription').trumbowyg('html');
            vm.ProductsService.updateProduct(vm.productDetails).then(function(response) {
				if (response) alert('Products updated !');
            });
        };
        
        vm.deleteProduct = function(id) {
            var answer = confirm('Are you sure you want to delete this entry ?');
            if (answer) {
                var data = {
                    id: id
                };
                vm.ProductsService.deleteProduct(data).then(function(response) {
                    response === 1 ? $location.path('/admin') : '';
                });
            }
        };
				
		vm.selectMainImage = function(event, index) {
			var container = event.target.parentNode;
			$('.main_product').removeClass('main_product');
			$(container).addClass('main_product');
			assignMainImage(index);
		};
		
		vm.deleteImages = function(index) {
			vm.productsImages.splice(index, 1);
		};
		
		function assignMainImage(index) {
			for(var i = 0; i < vm.productsImages.length; i++) {
				i === index ? vm.productsImages[i].isMain = true : vm.productsImages[i].isMain = false;
			}
		}
		
		function read_image(file) {
            var fileReader = new FileReader();
        	fileReader.onload = function(fileLoadedEvent) {
				var image = {
					file: fileLoadedEvent.target.result,
					isMain: vm.productsImages.length === 0
				};
				vm.productsImages.push(image);
				$scope.$apply();
			};
        	fileReader.readAsDataURL(file);
        }
		
    }
    
    angular.module('mainApp')
    .component('adminProductManager', {
        templateUrl: 'app/components/admin/products/manager.partial.html',
        controller: ['$routeParams', '$location', '$scope', 'ProductsService', adminProductManagerController]
    });
})();
(function() {

    function adminProductsController($location, ProductsService) {
        var vm = this;
        ProductsService.getProducts(1000000).then(function(response) {
            vm.products = response;
        });
        
        vm.goToManager = function(id) {
            var path = 'admin/product/' + id;
            $location.path(path); 
        };
        
    }

    angular.module('mainApp')
    .component('adminProducts', {
        templateUrl: '/app/components/admin/products/products.partial.html',
        controller: ['$location', 'ProductsService', adminProductsController]
    });
})();
(function(){
    
    function typesController($window, ProductsService) {
        vm = this;
        vm.showCreate = false;
        vm.createName = '';
        vm.editMode = 0;
        
        ProductsService.getProductTypes().then(function(response) {
            vm.types = response;
        });
        
        vm.toggleCreate = function() {
            vm.showCreate = true;
        };
        
        vm.saveType = function() {
            ProductsService.createType(vm.createName).then(function(response) {
                response === 1 ? $window.location.reload() : '';
            });
        };
        
        vm.deleteType = function(id) {
            var answer = confirm('Are you sure you want to delete this entry ?');
            if (answer) {
                ProductsService.deleteType(id).then(function(response) {
                    response === 1 ? $window.location.reload() : '';
                });
            }
        };

        vm.edit = function(id, name) {
            var data = {
                id: id,
                name: name
            };
            ProductsService.editType(data).then(function(response) {
                response === 1 ? $window.location.reload() : '';
            });
        }

        vm.editor_toggle = function(id) {
            vm.editMode = id;
        };
    }
    
    angular.module('mainApp')
    .component('typesManager', {
        templateUrl: '/app/components/admin/products/types.partial.html',
        controller: ['$window', 'ProductsService', typesController]
    });
})();
(function() {
    
    function adminUsersCtrl($location, AuthService) {
        var vm = this;
        
        AuthService.getAllUsers().then(function(response) {
            console.log('Users components =>', response);
            vm.users = response;
        });
        
    }
    
    angular.module('mainApp')
    .component('adminUsers', {
        bindings: {},
        templateUrl: 'app/components/admin/users/users.partial.html',
        controller: ['$location', 'AuthService', adminUsersCtrl]
    });
})();